import { User } from './../models/user.model';
import { Component, OnInit, ElementRef, ViewChild, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Login } from '../models/login.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { Observable, Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  @Output() showComponent: EventEmitter<boolean> = new EventEmitter<boolean>();

  loginForm: FormGroup;
  userSub: Subscription;

  constructor(private authService: AuthService, private router: Router) {
    this.loginForm = new FormGroup({
      username: new FormControl('', [Validators.required, Validators.pattern('[a-z0-9A-Z]{6,12}')]),
      password: new FormControl('', [Validators.required, Validators.pattern('[a-z0-9A-Z]{6,12}')]),
    });
  }

  ngOnInit(): void {

  }
  ngOnDestroy(): void {
    if (this.userSub) {
      this.userSub.unsubscribe();
    }

  }

  public onLoginFormSubmit(): void {
    const data = this.loginForm.value;
    if (this.loginForm.invalid) {
      window.alert('Form is not valid!');
      return;
    }
    const obsUser: Observable<User> = this.authService.loginUser(
      this.loginForm.value.username,
      this.loginForm.value.password,
    );
    const sub: Subscription = obsUser.subscribe((login: User) => {
      console.log(login);
      // redirekcija na komponentu za Usera
      this.router.navigate(['/user']);

    }, (err) => {
      window.alert('Wrong username or password, try again!');
    });
    this.userSub = sub;

  }

  public onShowRegistration(): void {
    const show = true;
    this.showComponent.emit(show);

  }

}
