import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { Observable } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';

import { StoreProduct } from '../models/store-product';
import { Product } from '../../old-products/models/product';
import { HttpErrorHandler } from '../../utils/http-error-handler.model';

@Injectable({
  providedIn: 'root',
})
export class StoreService extends HttpErrorHandler {
  private items: Product[] = [];
  private readonly ordersUrl = 'http://localhost:5001/storeProducts/';

  constructor(private http: HttpClient, router: Router) {
    super(router);
  }

  public addToCart(product: Product): void {
    this.items.push(product);
  }

  public getItems(): Product[] {
    return this.items;
  }

  public clearCart(): Product[] {
    this.items = [];
    return this.items;
  }


  public createAnOrder(formData): Observable<StoreProduct> {
    const body = { ...formData, products: this.items };
    return this.http
      .post<StoreProduct>(this.ordersUrl, body)
      .pipe(catchError(super.handleError()));
  }

  getProducts(): Observable<StoreProduct[]> {
    return this.http
      .get<StoreProduct[]>(this.ordersUrl)
      .pipe(catchError(super.handleError()));
  }

  getProductById(id: string): Observable<StoreProduct> {
    return this.http
      .get<StoreProduct>(this.ordersUrl + id)
      .pipe(catchError(super.handleError()));
  }
}