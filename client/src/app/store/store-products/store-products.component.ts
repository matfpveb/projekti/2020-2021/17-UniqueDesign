import { Component, Input, OnInit } from '@angular/core';
import { StoreProduct } from '../models/store-product';

@Component({
  selector: 'app-store-products',
  templateUrl: './store-products.component.html',
  styleUrls: ['./store-products.component.css']
})
export class StoreProductsComponent implements OnInit {

  @Input()
  products: StoreProduct[] = [];

  constructor() { }

  ngOnInit(): void {
  }

}
