import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { JwtService } from 'src/app/common/services/jwt.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DesignService {
  private readonly clothingItemsUrl = 'http://localhost:5001/clothingItems/';
  private readonly productsUrl = 'http://localhost:5001/products/';
  private readonly imgToolsUrl = 'http://localhost:5001/imageTools/';
  private readonly textToolsUrl = 'http://localhost:5001/textTools/';

  constructor(private http: HttpClient, private jwtService: JwtService) {}

  public getClothingItems() {
    const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtService.getToken()}`);
    return this.http
      .get<any[]>(this.clothingItemsUrl, { headers })
      .pipe(map((pojos) => pojos));
  }

  public getClothingItemsById(id) {
    const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtService.getToken()}`);
    return this.http
      .get<any[]>(this.clothingItemsUrl + id, { headers })
      .pipe(map((pojos) => pojos));
  }

  public getProductById(productId) {
    const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtService.getToken()}`);
    return this.http
      .get<any>(this.productsUrl + productId, { headers })
      .pipe(map((pojos) => pojos));
  }

  public postProduct(newProduct) {
    const body = {
      ... newProduct,
      idUser: this.jwtService.getDataFromToken() ? this.jwtService.getDataFromToken()['username'] : null,
    };

    const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtService.getToken()}`);

    return this.http.post<any>(this.productsUrl, body, { headers }).pipe(
      map((pojo) => pojo),
    );
  }

  public getImgToolsByProductId(productId) {
    const params = new HttpParams().append('productId', productId);
    const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtService.getToken()}`);
    return this.http
      .get<any[]>(this.imgToolsUrl, { headers, params })
      .pipe(map((pojos) => pojos));
  }

  public postImgTool(newImgTool) {
    const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtService.getToken()}`);

    return this.http.post<any>(this.imgToolsUrl, newImgTool, { headers }).pipe(
      map((pojo) => pojo));
  }

  public postTextTool(newTextTool) {
    const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtService.getToken()}`);

    return this.http.post<any>(this.textToolsUrl, newTextTool, { headers }).pipe(
      map((pojo) => pojo));
  }

  public getTextToolsByProductId(productId) {
    const params = new HttpParams().append('productId', productId);
    const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtService.getToken()}`);
    return this.http
      .get<any[]>(this.textToolsUrl, { headers, params })
      .pipe(map((pojos) => pojos));
  }
}
