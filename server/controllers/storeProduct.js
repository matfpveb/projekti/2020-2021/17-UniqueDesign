const mongoose = require('mongoose');

const StoreProduct = require('../models/storeProduct');

module.exports.getStoreProducts = async function (req, res, next) {
  try {
    const storeProducts = await StoreProduct.find({}).exec();
    res.status(200).json(storeProducts);
  } catch (err) {
    next(err);
  }
};

module.exports.createAnStoreProduct = async function (req, res, next) {
  const storeProduct = new StoreProduct({
    _id: new mongoose.Types.ObjectId(),
    products: req.body.products.map(p => p._id),
    name: req.body.name,
    address: req.body.address,
    email: req.body.email,
  });
  try {
    const savedObject = await storeProduct.save();
    res.status(201).json(savedObject);
  } catch (err) {
    next(err);
  }
};

module.exports.getAnStoreProductById = async function (req, res, next) {
  const storeProductId = req.params.storeProductId;

  try {
    const storeProduct = await StoreProduct.findById(storeProductId).populate('products').exec();
    if (!storeProduct) {
      return res
        .status(404)
        .json({ message: 'The product with given id does not exist' });
    }
    res.status(200).json(storeProduct);
  } catch (err) {
    next(err);
  }
};

