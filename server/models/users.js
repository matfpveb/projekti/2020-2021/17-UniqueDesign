const mongoose = require('mongoose');
const uuid = require('uuid')

const usersSchema = new mongoose.Schema({
    _id:mongoose.Schema.Types.ObjectId,
    idUser: {
        type: String,
        default: uuid.v4(),
    },
    name: {
        type: String,
        required: true,
    },
    surname: {
        type: String,
        required: true,
    },
    username: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    status: {
        type: String,
        default: 'active',
    },
    admin: {
        type: Boolean,
        default: false,
    },
    imgUrl:{
        type: String,
        default:'default-user1.jpg',
    },
});

const usersModel = mongoose.model('users', usersSchema);
module.exports = usersModel;
